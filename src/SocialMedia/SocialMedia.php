<?php

namespace Solnet\SocialMedia;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class SocialMedia extends DataObject
{
    use Configurable;

    private static $table_name = 'SocialMedia';

    private static $db = [
        'Title' => 'Varchar(100)',
        'Logo' => 'Varchar(100)',
        'Link' => 'Text',
        'SortOrder' => 'Int',
    ];

    private static $has_one = [
        'Config' => 'SilverStripe\SiteConfig\SiteConfig'
    ];

    private static $summary_fields = [
        'Title' => 'Title'
    ];

    private static $default_sort = [
        'SortOrder',
    ];

    /**
     * @config
     * The options available for the Logo Class field.
     */
    private static $icon_options = [
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('ConfigID');
        $fields->removeByName('SortOrder');

        $options = 

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create(
                    'Title',
                    _t('SolnetSocialMedia.SocialMedia_Title_Title', 'Social Media Name')
                ),
                $logoField = DropdownField::create(
                    'Logo',
                    _t('SolnetSocialMedia.SocialMedia_Logo_Title', 'Logo'),
                    $this->config()->icon_options
                )
                ->setDescription(
                    _t(
                        'SolnetSocialMedia.SocialMedia_Logo_Description',
                        'Select the Social Media logo here.'
                    )
                ),
                TextField::create(
                    'Link',
                    _t('SolnetSocialMedia.SocialMedia_Link_Title', 'Link')
                ),
            ]
        );

        $logoField->setEmptyString(
            _t('SolnetSocialMedia.SocialMedia_Logo_EmptyString', '(Select a logo)')
        );

        return $fields;
    }

    public function getCurYear()
    {
        $year =  date('Y');
        return $year;
    }
}
