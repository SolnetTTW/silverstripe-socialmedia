<?php

namespace Solnet\SocialMedia\Config;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class SiteConfigExtension extends DataExtension
{
    private static $has_many = [
        'Socials' => 'Solnet\SocialMedia\SocialMedia'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.Footer',
            [
                GridField::create(
                    'Socials',
                    _t('SolnetSocialMedia.Config_Socials_Title', 'Social Media'),
                    $this->owner->Socials(),
                    GridFieldConfig_RelationEditor::create()
                    ->addComponent(new GridFieldOrderableRows('SortOrder'))
                ),
            ]
        );
    }
}
